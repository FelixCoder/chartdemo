//
//  FYPieView.h
//  FYChartDemo
//
//  Created by Felix Yin on 2017/6/1.
//  Copyright © 2017年 Felix Yin. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FYPieView;
@protocol FYPieViewDelegate <NSObject>

/**
 *  旋转到重心位置
 *
 *  @param fyPieView 饼图
 *  @param index     旋转到重心位置的区块索引
 */
- (void)fyPieView:(FYPieView *) fyPieView selectedIndex:(NSInteger) index;

@end

@interface FYPieView : UIView

@property (nonatomic, assign) id<FYPieViewDelegate> delegate;

/**
 *  更新pie view 区块数据
 *
 *  @param titleArray 区块数据集合
 */
- (void)updatePieViewData:(NSArray *) titleArray;


@end
