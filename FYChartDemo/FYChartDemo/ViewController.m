//
//  ViewController.m
//  FYChartDemo
//
//  Created by Felix Yin on 2017/5/12.
//  Copyright © 2017年 Felix Yin. All rights reserved.
//

#import "ViewController.h"
#import <Charts/Charts-Swift.h>
#import "UIImageView+WebCache.h"
#import "FYPieView.h"

@interface ViewController ()<FYPieViewDelegate>

@property (nonatomic, strong)  FYPieView *pieChartView;
@property (nonatomic, strong) UIImageView *downloadImageView;
@property (nonatomic, copy) NSString *hh;
@property (nonatomic, strong) NSMutableArray *titleArray;
@property (nonatomic, strong) UILabel *titleLbl;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    ,@"Jack",@"Felix",@"Jose",@"Ocoka",@"Kokii",@"Vovw",@"Kokii090",@"Vovw45",@"JJJ"]
    self.titleArray = [[NSMutableArray alloc] initWithArray:@[@"Tom",@"Vooal",@"jal;fjlk;"]];
    [self.view addSubview:self.pieChartView];
    [self.pieChartView updatePieViewData:_titleArray];
}


#pragma mark FYPieView Delegate Override

- (void)fyPieView:(FYPieView *)fyPieView selectedIndex:(NSInteger)index{
    NSLog(@"value ==== value ==== %@",self.titleArray[index]);
}

#pragma mark Lazy Loading

- (FYPieView *)pieChartView{
    if (!_pieChartView) {
        _pieChartView = [[FYPieView alloc] initWithFrame:CGRectMake(0, 10, 200, 200)];
        _pieChartView.delegate = self;
    }
    return _pieChartView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
