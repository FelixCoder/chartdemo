//
//  AppDelegate.h
//  SwiftAndOC
//
//  Created by Felix Yin on 2017/5/15.
//  Copyright © 2017年 Felix Yin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

